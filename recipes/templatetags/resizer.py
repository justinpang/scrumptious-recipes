from django import template

register = template.Library()


def resize_to(ingredients, target):
    servings = ingredients.recipe.servings
    if servings is not None and target is not None:
        try:
            ratio = int(target) / servings
            return ingredients.amount * ratio
        except ValueError:
            pass
    return ingredients.amount

    # try:
    # ratio = target / servings
    # return ingredients.amount * ratio
    # except:
    # pass


register.filter(resize_to)
