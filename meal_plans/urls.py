from django.urls import path

from meal_plans.views import (
    MealPlanListView,
    MealPlanDetailView,
    MealPlanDeleteView,
    MealPlanCreateView,
    MealPlanUpdateView,
)
   

urlpatterns = [
    path("", MealPlanListView.as_view(), name="mealplans_list"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="mealplan_detail"),
    path("<int:pk>/delete/", MealPlanDeleteView.as_view(), name="mealplan_delete"),
    path("create/", MealPlanCreateView.as_view(), name="mealplan_create"),
    path("<int:pk>/edit/", MealPlanUpdateView.as_view(), name="mealplan_edit"),
]